## Ubuntu Touch's system update plugin for Lomiri system settings

This plugin provides the system update plugin to lomiri-system-settings.
The plugin contains the system update section, which handles both system
updates and (click) application updates.

This package also comes with the push helper for handling update notifications.
