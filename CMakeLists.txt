project(lomiri-system-settings-system-update C CXX)
cmake_minimum_required(VERSION 3.4)

if(${PROJECT_BINARY_DIR} STREQUAL ${PROJECT_SOURCE_DIR})
   message(FATAL_ERROR "In-tree build attempt detected, aborting. Set your build dir outside your source dir, delete CMakeCache.txt from source root and try again.")
endif()

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
include(FindPkgConfig)
include(GNUInstallDirs)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt5
    COMPONENTS
        Core
        DBus
        Gui
        Network
        Quick
        Sql
    REQUIRED
)

pkg_check_modules(LomiriSystemSettings
    REQUIRED IMPORTED_TARGET
    LomiriSystemSettings
)
pkg_get_variable(PLUGIN_MANIFEST_DIR LomiriSystemSettings plugin_manifest_dir)
pkg_get_variable(PLUGIN_MODULE_DIR LomiriSystemSettings plugin_module_dir)
pkg_get_variable(PLUGIN_PRIVATE_MODULE_DIR LomiriSystemSettings plugin_private_module_dir)
pkg_get_variable(PLUGIN_QML_DIR LomiriSystemSettings plugin_qml_dir)

pkg_check_modules(AptPkg
    REQUIRED IMPORTED_TARGET
    apt-pkg
)

add_subdirectory(plugins/system-update)
add_subdirectory(push-helper)

add_subdirectory(po)

option(ENABLE_TESTS "Build tests" ON)
if(ENABLE_TESTS)
    enable_testing()
    find_package(Qt5 COMPONENTS Test REQUIRED)
    pkg_check_modules(QtDBusMock
        REQUIRED IMPORTED_TARGET
        libqtdbusmock-1
    )
    pkg_check_modules(QtDBusTest
        REQUIRED IMPORTED_TARGET
        libqtdbustest-1
    )
    pkg_check_modules(GLib
        REQUIRED IMPORTED_TARGET
        glib-2.0
    )
    add_subdirectory(tests)
endif()
