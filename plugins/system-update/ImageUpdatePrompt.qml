/*
 * This file is part of system-settings
 *
 * Copyright (C) 2016 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import "i18nd.js" as I18nd

Dialog {
    id: dialogueInstall
    objectName: "imagePrompt"

    property alias havePowerForUpdate: installBtn.visible

    signal requestSystemUpdate()

    title: I18nd.tr("Update System")
    text: havePowerForUpdate
            ? I18nd.tr("The device needs to restart to install the system update.")
            : I18nd.tr("Connect the device to power before installing the system update.")

    Button {
        id: installBtn
        objectName: "imagePromptInstall"
        text: I18nd.tr("Restart & Install")
        color: theme.palette.selected.focus
        onClicked: {
            dialogueInstall.requestSystemUpdate();
            PopupUtils.close(dialogueInstall);
        }
    }

    Button {
        objectName: "imagePromptCancel"
        text: I18nd.tr("Cancel")
        onClicked: PopupUtils.close(dialogueInstall)
    }
}
