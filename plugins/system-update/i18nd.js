/*
 * This file is part of lomiri-system-settings-system-update
 *
 * Copyright (C) 2021 UBports Foundation.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// A simple shortcut to use our own translation domain.
// Note that "i18n" is a context property from UITK, so import that first.
function tr(...args) {
    return i18n.dtr("lomiri-system-settings-system-update", ...args);
}
