# Gaelic; Scottish translation for lomiri-system-settings
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-system-settings-system-update package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
# Michael Bauer <fios@akerbeltz.org>, 2014.
# GunChleoc <fios@foramnagaidhlig.net>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-system-update\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-18 11:12-0600\n"
"PO-Revision-Date: 2014-10-20 19:38+0000\n"
"Last-Translator: GunChleoc <Unknown>\n"
"Language-Team: Fòram na Gàidhlig\n"
"Language: gd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : "
"(n > 2 && n < 20) ? 2 : 3;\n"
"X-Launchpad-Export-Date: 2015-07-16 05:40+0000\n"
"X-Generator: Launchpad (build 17628)\n"

#: ../plugins/system-update/Configuration.qml:54
#: ../plugins/system-update/UpdateSettings.qml:58
msgid "Never"
msgstr "Cha robh gin"

#: ../plugins/system-update/ImageUpdatePrompt.qml:48
msgid "Cancel"
msgstr "Sguir dheth"

#: ../plugins/system-update/UpdateSettings.qml:64
msgid "Unknown"
msgstr "Neo-aithnichte"

#: ../plugins/system-update/InstallationFailed.qml:28
msgid "OK"
msgstr "Ceart ma-thà"

#: ../plugins/system-update/ChangelogExpander.qml:42
#, fuzzy, qt-format
msgid "Version %1"
msgstr "Tionndadh: "

#: ../plugins/system-update/ChannelSettings.qml:32
msgid "Channel settings"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:57
#: ../plugins/system-update/UpdateSettings.qml:74
#, fuzzy
msgid "Stable"
msgstr "Tablaid"

#: ../plugins/system-update/ChannelSettings.qml:57
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Release candidate"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:57
#: ../plugins/system-update/UpdateSettings.qml:74
#, fuzzy
msgid "Development"
msgstr "Modh an luchd-leasachaidh"

#: ../plugins/system-update/ChannelSettings.qml:99
msgid "Fetching channels"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:108
msgid "Channel to get updates from:"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:124
msgid "Switching channel"
msgstr ""

#: ../plugins/system-update/Configuration.qml:30
#: ../plugins/system-update/UpdateSettings.qml:55
msgid "Auto download"
msgstr "Luchdadh a-nuas fèin-obrachail"

#: ../plugins/system-update/Configuration.qml:36
msgid "Download future updates automatically:"
msgstr "Luchdaich a-nuas ùrachaidhean gu fèin-obrachail o seo a-mach:"

#: ../plugins/system-update/Configuration.qml:55
msgid "When on WiFi"
msgstr "Air WiFi"

#: ../plugins/system-update/Configuration.qml:57
msgid "On any data connection"
msgstr "Air ceangal dàta sam bith"

#: ../plugins/system-update/Configuration.qml:58
msgid "Data charges may apply."
msgstr "Dh'fhaoidte gun èirich cosgaisean dàta."

#: ../plugins/system-update/DownloadHandler.qml:174
#: ../plugins/system-update/InstallationFailed.qml:25
msgid "Installation failed"
msgstr "Dh'fhàillig an stàladh"

#: ../plugins/system-update/FirmwareUpdate.qml:36
#: ../plugins/system-update/FirmwareUpdate.qml:157
msgid "Firmware Update"
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:133
#, fuzzy
msgid "There is a firmware update available!"
msgstr "Tha ùrachaidhean ri fhaighinn"

#: ../plugins/system-update/FirmwareUpdate.qml:134
#, fuzzy
msgid "Firmware is up to date!"
msgstr "Tha am bathar-bog cho ùr 's a ghabhas"

#: ../plugins/system-update/FirmwareUpdate.qml:169
#, fuzzy
msgid "The device will restart automatically after installing is done."
msgstr ""
"Feumaidh am fòn tòiseachadh as ùr gus ùrachadh an t-siostaim a chur an sàs."

#: ../plugins/system-update/FirmwareUpdate.qml:185
#, fuzzy
msgid "Install and restart now"
msgstr "Stàlaich ⁊ ath-thòisich"

#: ../plugins/system-update/FirmwareUpdate.qml:228
msgid ""
"Downloading and Flashing firmware updates, this could take a few minutes..."
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:235
#, fuzzy
msgid "Checking for firmware update"
msgstr "A' lorg ùrachaidhean…"

#: ../plugins/system-update/GlobalUpdateControls.qml:84
msgid "Checking for updates…"
msgstr "A' lorg ùrachaidhean…"

#: ../plugins/system-update/GlobalUpdateControls.qml:89
msgid "Stop"
msgstr ""

#. TRANSLATORS: %1 is number of software updates available.
#: ../plugins/system-update/GlobalUpdateControls.qml:116
#, fuzzy, qt-format
msgid "%1 update available"
msgid_plural "%1 updates available"
msgstr[0] "Tha ùrachaidhean ri fhaighinn"
msgstr[1] "Tha ùrachaidhean ri fhaighinn"
msgstr[2] "Tha ùrachaidhean ri fhaighinn"
msgstr[3] "Tha ùrachaidhean ri fhaighinn"

#: ../plugins/system-update/GlobalUpdateControls.qml:126
#, fuzzy
msgid "Update all…"
msgstr "Tha ùrachaidhean ri fhaighinn"

#: ../plugins/system-update/GlobalUpdateControls.qml:128
#, fuzzy
msgid "Update all"
msgstr "Ùraich"

#: ../plugins/system-update/ImageUpdatePrompt.qml:30
msgid "Update System"
msgstr "Ùraich an siostam"

#: ../plugins/system-update/ImageUpdatePrompt.qml:32
#, fuzzy
msgid "The device needs to restart to install the system update."
msgstr ""
"Feumaidh am fòn tòiseachadh as ùr gus ùrachadh an t-siostaim a chur an sàs."

#: ../plugins/system-update/ImageUpdatePrompt.qml:33
#, fuzzy
msgid "Connect the device to power before installing the system update."
msgstr ""
"Ceangail am fòn ris a' chumhachd mus tòisich thu air ùrachadh an t-siostaim "
"a stàladh."

#: ../plugins/system-update/ImageUpdatePrompt.qml:38
msgid "Restart & Install"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../plugins/system-update/PageComponent.qml:37 ../build/po/settings.js:348
msgid "Updates"
msgstr "Ùrachaidhean"

#: ../plugins/system-update/PageComponent.qml:163
#: ../plugins/system-update/ReinstallAllApps.qml:150
#, fuzzy
msgid "Connect to the Internet to check for updates."
msgstr "Ceangail ris an eadar-lìon gus sùil a thoirt airson ùrachaidhean"

#: ../plugins/system-update/PageComponent.qml:165
#: ../plugins/system-update/ReinstallAllApps.qml:152
msgid "Software is up to date"
msgstr "Tha am bathar-bog cho ùr 's a ghabhas"

#: ../plugins/system-update/PageComponent.qml:168
#: ../plugins/system-update/ReinstallAllApps.qml:155
msgid "The update server is not responding. Try again later."
msgstr ""

#: ../plugins/system-update/PageComponent.qml:177
#: ../plugins/system-update/ReinstallAllApps.qml:164
#, fuzzy
msgid "Updates Available"
msgstr "Tha ùrachaidhean ri fhaighinn"

#: ../plugins/system-update/PageComponent.qml:311
#, fuzzy
msgid "Recent updates"
msgstr "Thoir sùil airson ùrachaidhean"

#: ../plugins/system-update/PageComponent.qml:374
#: ../plugins/system-update/UpdateSettings.qml:32
#, fuzzy
msgid "Update settings"
msgstr "Roghainnean an t-siostaim"

#: ../plugins/system-update/ReinstallAllApps.qml:36
#: ../plugins/system-update/ReinstallAllApps.qml:95
#: ../plugins/system-update/UpdateSettings.qml:88
msgid "Reinstall all apps"
msgstr ""

#: ../plugins/system-update/ReinstallAllApps.qml:85
msgid ""
"Use this to get all of the latest apps, typically needed after a major "
"system upgrade, "
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:119
msgid "Retry"
msgstr "Feuch ris a-rithist"

#: ../plugins/system-update/UpdateDelegate.qml:124
msgid "Update"
msgstr "Ùraich"

#: ../plugins/system-update/UpdateDelegate.qml:126
msgid "Download"
msgstr "Luchdaich a-nuas"

#: ../plugins/system-update/UpdateDelegate.qml:132
msgid "Resume"
msgstr "Lean air"

#: ../plugins/system-update/UpdateDelegate.qml:139
msgid "Pause"
msgstr "Cuir 'na stad"

#: ../plugins/system-update/UpdateDelegate.qml:143
msgid "Install…"
msgstr "Stàlaich…"

#: ../plugins/system-update/UpdateDelegate.qml:145
msgid "Install"
msgstr "Stàlaich"

#: ../plugins/system-update/UpdateDelegate.qml:149
msgid "Open"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:228
msgid "Installing"
msgstr "'Ga stàladh"

#: ../plugins/system-update/UpdateDelegate.qml:232
#, fuzzy
msgid "Paused"
msgstr "Cuir 'na stad"

#: ../plugins/system-update/UpdateDelegate.qml:235
#, fuzzy
msgid "Waiting to download"
msgstr "Luchdadh a-nuas fèin-obrachail"

#: ../plugins/system-update/UpdateDelegate.qml:238
msgid "Downloading"
msgstr "'Ga luchdadh a-nuas"

#. TRANSLATORS: %1 is the human readable amount
#. of bytes downloaded, and %2 is the total to be
#. downloaded.
#: ../plugins/system-update/UpdateDelegate.qml:274
#, qt-format
msgid "%1 of %2"
msgstr "%1 à %2"

#: ../plugins/system-update/UpdateDelegate.qml:278
#, fuzzy
msgid "Downloaded"
msgstr "Luchdaich a-nuas"

#: ../plugins/system-update/UpdateDelegate.qml:281
msgid "Installed"
msgstr "Air a stàladh"

#. TRANSLATORS: %1 is the date at which this
#. update was applied.
#: ../plugins/system-update/UpdateDelegate.qml:286
#, fuzzy, qt-format
msgid "Updated %1"
msgstr "Ùraich"

#: ../plugins/system-update/UpdateDelegate.qml:310
#, fuzzy
msgid "Update failed"
msgstr "Tha ùrachaidhean ri fhaighinn"

#: ../plugins/system-update/UpdateSettings.qml:60
msgid "On WiFi"
msgstr "Air WiFi"

#: ../plugins/system-update/UpdateSettings.qml:62
msgid "Always"
msgstr "An-còmhnaidh"

#: ../plugins/system-update/UpdateSettings.qml:70
#, fuzzy
msgid "Channels"
msgstr "Atharraich"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:26
msgid "software"
msgstr "software;bathar-bog"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:108
msgid "automatic"
msgstr "fèin-obrachail"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:210
msgid "click"
msgstr "briogadh"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:220
msgid "apps"
msgstr "aplacaidean"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:350
msgid "system"
msgstr "system;siostam"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:354
msgid "update"
msgstr "update;ùrachadh"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:358
#, fuzzy
msgid "application"
msgstr "capitalization;cèis;litrichean;mòra;beaga"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:362
msgid "download"
msgstr "luchdadh a-nuas"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:364
msgid "upgrade"
msgstr "ùrachadh"

