# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-system-update\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-18 11:12-0600\n"
"PO-Revision-Date: 2020-10-26 22:41+0000\n"
"Last-Translator: Adria <adriamartinmor@gmail.com>\n"
"Language-Team: Sardinian <https://translate.ubports.com/projects/ubports/"
"system-settings/sc/>\n"
"Language: sc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"

#: ../plugins/system-update/Configuration.qml:54
#: ../plugins/system-update/UpdateSettings.qml:58
msgid "Never"
msgstr "Mai"

#: ../plugins/system-update/ImageUpdatePrompt.qml:48
msgid "Cancel"
msgstr "Annulla"

#: ../plugins/system-update/UpdateSettings.qml:64
msgid "Unknown"
msgstr "Disconnotu"

#: ../plugins/system-update/InstallationFailed.qml:28
msgid "OK"
msgstr "AB"

#: ../plugins/system-update/ChangelogExpander.qml:42
#, qt-format
msgid "Version %1"
msgstr "Versione %1"

#: ../plugins/system-update/ChannelSettings.qml:32
msgid "Channel settings"
msgstr "Cunfiguratzione de su canale"

#: ../plugins/system-update/ChannelSettings.qml:57
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Stable"
msgstr "Istàbile"

#: ../plugins/system-update/ChannelSettings.qml:57
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Release candidate"
msgstr "Versione candidada"

#: ../plugins/system-update/ChannelSettings.qml:57
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Development"
msgstr "Modalidade de isvilupu"

#: ../plugins/system-update/ChannelSettings.qml:99
msgid "Fetching channels"
msgstr "Recuperende is canales"

#: ../plugins/system-update/ChannelSettings.qml:108
msgid "Channel to get updates from:"
msgstr "Canale pro otènnere atualizatziones:"

#: ../plugins/system-update/ChannelSettings.qml:124
msgid "Switching channel"
msgstr "Cambiende su canale"

#: ../plugins/system-update/Configuration.qml:30
#: ../plugins/system-update/UpdateSettings.qml:55
msgid "Auto download"
msgstr "Iscarrigamentu automàticu"

#: ../plugins/system-update/Configuration.qml:36
msgid "Download future updates automatically:"
msgstr "Iscàrriga atualizatziones imbenientes in automàticu:"

#: ../plugins/system-update/Configuration.qml:55
msgid "When on WiFi"
msgstr "Cando so impreende WiFi"

#: ../plugins/system-update/Configuration.qml:57
msgid "On any data connection"
msgstr "Cun cale si siat connessione de datos"

#: ../plugins/system-update/Configuration.qml:58
msgid "Data charges may apply."
msgstr "Bi diant pòdere èssere ispesas de datos."

#: ../plugins/system-update/DownloadHandler.qml:174
#: ../plugins/system-update/InstallationFailed.qml:25
msgid "Installation failed"
msgstr "Faddina in s'installatzione"

#: ../plugins/system-update/FirmwareUpdate.qml:36
#: ../plugins/system-update/FirmwareUpdate.qml:157
msgid "Firmware Update"
msgstr "Atualizatzione firmware"

#: ../plugins/system-update/FirmwareUpdate.qml:133
msgid "There is a firmware update available!"
msgstr "%1 atualizatzione de firmware a disponimentu!"

#: ../plugins/system-update/FirmwareUpdate.qml:134
msgid "Firmware is up to date!"
msgstr "Su firmware est atualizadu."

#: ../plugins/system-update/FirmwareUpdate.qml:169
msgid "The device will restart automatically after installing is done."
msgstr ""
"Su dispositivu s'at a torrare a aviare in automàticu a pustis de "
"s'installatzione."

#: ../plugins/system-update/FirmwareUpdate.qml:185
msgid "Install and restart now"
msgstr "Installa e torra a aviare immoe"

#: ../plugins/system-update/FirmwareUpdate.qml:228
msgid ""
"Downloading and Flashing firmware updates, this could take a few minutes..."
msgstr ""
"Iscarrighende e installende atualizatziones de firmware. Bi podet bòlere "
"carchi minutu..."

#: ../plugins/system-update/FirmwareUpdate.qml:235
msgid "Checking for firmware update"
msgstr "Controllende atualizatziones de firmware"

#: ../plugins/system-update/GlobalUpdateControls.qml:84
msgid "Checking for updates…"
msgstr "Controllende atualizatziones…"

#: ../plugins/system-update/GlobalUpdateControls.qml:89
msgid "Stop"
msgstr "Firma"

#. TRANSLATORS: %1 is number of software updates available.
#: ../plugins/system-update/GlobalUpdateControls.qml:116
#, qt-format
msgid "%1 update available"
msgid_plural "%1 updates available"
msgstr[0] "%1 atualizatzione a disponimentu"
msgstr[1] "%1 atualizatziones a disponimentu"

#: ../plugins/system-update/GlobalUpdateControls.qml:126
msgid "Update all…"
msgstr "Atualiza totu…"

#: ../plugins/system-update/GlobalUpdateControls.qml:128
msgid "Update all"
msgstr "Atualiza totu"

#: ../plugins/system-update/ImageUpdatePrompt.qml:30
msgid "Update System"
msgstr "Atualiza su sistema"

#: ../plugins/system-update/ImageUpdatePrompt.qml:32
msgid "The device needs to restart to install the system update."
msgstr ""
"Su dispositivu depet èssere torradu a aviare pro installare s'atualizatzione "
"de su sistema."

#: ../plugins/system-update/ImageUpdatePrompt.qml:33
msgid "Connect the device to power before installing the system update."
msgstr ""
"Connete su dispositivu a su carrigadore in antis de installare "
"s'atualizatzione de su sistema."

#: ../plugins/system-update/ImageUpdatePrompt.qml:38
msgid "Restart & Install"
msgstr "Torra a aviare e installa"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../plugins/system-update/PageComponent.qml:37 ../build/po/settings.js:348
msgid "Updates"
msgstr "Atualizatziones"

#: ../plugins/system-update/PageComponent.qml:163
#: ../plugins/system-update/ReinstallAllApps.qml:150
msgid "Connect to the Internet to check for updates."
msgstr "Connete·ti a internet pro chircare atualizatziones."

#: ../plugins/system-update/PageComponent.qml:165
#: ../plugins/system-update/ReinstallAllApps.qml:152
msgid "Software is up to date"
msgstr "Programmas atualizados"

#: ../plugins/system-update/PageComponent.qml:168
#: ../plugins/system-update/ReinstallAllApps.qml:155
msgid "The update server is not responding. Try again later."
msgstr ""
"Su server de atualizatziones non rispondet. Torra·bi a proare a pustis."

#: ../plugins/system-update/PageComponent.qml:177
#: ../plugins/system-update/ReinstallAllApps.qml:164
msgid "Updates Available"
msgstr "Atualizatziones a disponimentu"

#: ../plugins/system-update/PageComponent.qml:311
msgid "Recent updates"
msgstr "Atualizatziones reghentes"

#: ../plugins/system-update/PageComponent.qml:374
#: ../plugins/system-update/UpdateSettings.qml:32
msgid "Update settings"
msgstr "Cunfiguratzione de atualizatziones"

#: ../plugins/system-update/ReinstallAllApps.qml:36
#: ../plugins/system-update/ReinstallAllApps.qml:95
#: ../plugins/system-update/UpdateSettings.qml:88
msgid "Reinstall all apps"
msgstr "Torra a installare totu is aplicatziones"

#: ../plugins/system-update/ReinstallAllApps.qml:85
msgid ""
"Use this to get all of the latest apps, typically needed after a major "
"system upgrade, "
msgstr ""
"Imprea custu pro atualizare totu is aplicatziones a pustis de "
"un'atualizatzione importante de sistema, "

#: ../plugins/system-update/UpdateDelegate.qml:119
msgid "Retry"
msgstr "Torra·bi a proare"

#: ../plugins/system-update/UpdateDelegate.qml:124
msgid "Update"
msgstr "Atualiza"

#: ../plugins/system-update/UpdateDelegate.qml:126
msgid "Download"
msgstr "Iscàrriga"

#: ../plugins/system-update/UpdateDelegate.qml:132
msgid "Resume"
msgstr "Sighi"

#: ../plugins/system-update/UpdateDelegate.qml:139
msgid "Pause"
msgstr "Pàusa"

#: ../plugins/system-update/UpdateDelegate.qml:143
msgid "Install…"
msgstr "Installa…"

#: ../plugins/system-update/UpdateDelegate.qml:145
msgid "Install"
msgstr "Installa"

#: ../plugins/system-update/UpdateDelegate.qml:149
msgid "Open"
msgstr "Aberi"

#: ../plugins/system-update/UpdateDelegate.qml:228
msgid "Installing"
msgstr "Installende"

#: ../plugins/system-update/UpdateDelegate.qml:232
msgid "Paused"
msgstr "In pàusa"

#: ../plugins/system-update/UpdateDelegate.qml:235
msgid "Waiting to download"
msgstr "Isetende s'iscarrigamentu"

#: ../plugins/system-update/UpdateDelegate.qml:238
msgid "Downloading"
msgstr "Iscarrighende"

#. TRANSLATORS: %1 is the human readable amount
#. of bytes downloaded, and %2 is the total to be
#. downloaded.
#: ../plugins/system-update/UpdateDelegate.qml:274
#, qt-format
msgid "%1 of %2"
msgstr "%1 de %2"

#: ../plugins/system-update/UpdateDelegate.qml:278
msgid "Downloaded"
msgstr "Iscarrigadu"

#: ../plugins/system-update/UpdateDelegate.qml:281
msgid "Installed"
msgstr "Installadu"

#. TRANSLATORS: %1 is the date at which this
#. update was applied.
#: ../plugins/system-update/UpdateDelegate.qml:286
#, qt-format
msgid "Updated %1"
msgstr "Atualizadu su %1"

#: ../plugins/system-update/UpdateDelegate.qml:310
msgid "Update failed"
msgstr "Faddina in s'atualizatzione"

#: ../plugins/system-update/UpdateSettings.qml:60
msgid "On WiFi"
msgstr "Cando impreo WiFi"

#: ../plugins/system-update/UpdateSettings.qml:62
msgid "Always"
msgstr "Semper"

#: ../plugins/system-update/UpdateSettings.qml:70
msgid "Channels"
msgstr "Canales"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:26
msgid "software"
msgstr "programma"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:108
msgid "automatic"
msgstr "automàticu"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:210
msgid "click"
msgstr "clic"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:220
msgid "apps"
msgstr "aplicatziones"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:350
msgid "system"
msgstr "sistema"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:354
msgid "update"
msgstr "atualiza"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:358
msgid "application"
msgstr "aplicatzione"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:362
msgid "download"
msgstr "iscàrriga"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:364
msgid "upgrade"
msgstr "atualiza"

