# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the lomiri-system-settings-system-update package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-system-update\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-27 18:23+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ../plugins/system-update/ChangelogExpander.qml:43
#, qt-format
msgid "Version %1"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:33
msgid "Channel settings"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:58
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Development"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:58
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Release candidate"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:58
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Stable"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:95
msgid "Fetching channels"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:104
msgid "Channel to get updates from:"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:120
msgid "Switching channel"
msgstr ""

#: ../plugins/system-update/Configuration.qml:30
#: ../plugins/system-update/UpdateSettings.qml:55
msgid "Auto download"
msgstr ""

#: ../plugins/system-update/Configuration.qml:36
msgid "Download future updates automatically:"
msgstr ""

#: ../plugins/system-update/Configuration.qml:54
#: ../plugins/system-update/UpdateSettings.qml:58
msgid "Never"
msgstr ""

#: ../plugins/system-update/Configuration.qml:55
msgid "When on WiFi"
msgstr ""

#: ../plugins/system-update/Configuration.qml:57
msgid "On any data connection"
msgstr ""

#: ../plugins/system-update/Configuration.qml:58
msgid "Data charges may apply."
msgstr ""

#: ../plugins/system-update/DownloadHandler.qml:175
#: ../plugins/system-update/InstallationFailed.qml:26
msgid "Installation failed"
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:37
#: ../plugins/system-update/FirmwareUpdate.qml:158
msgid "Firmware Update"
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:134
msgid "There is a firmware update available!"
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:135
msgid "Firmware is up to date!"
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:170
msgid "The device will restart automatically after installing is done."
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:186
msgid "Install and restart now"
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:229
msgid ""
"Downloading and Flashing firmware updates, this could take a few minutes..."
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:236
msgid "Checking for firmware update"
msgstr ""

#: ../plugins/system-update/GlobalUpdateControls.qml:85
msgid "Checking for updates…"
msgstr ""

#: ../plugins/system-update/GlobalUpdateControls.qml:90
msgid "Stop"
msgstr ""

#. TRANSLATORS: %1 is number of software updates available.
#: ../plugins/system-update/GlobalUpdateControls.qml:117
#, qt-format
msgid "%1 update available"
msgid_plural "%1 updates available"
msgstr[0] ""
msgstr[1] ""

#: ../plugins/system-update/GlobalUpdateControls.qml:127
msgid "Update all…"
msgstr ""

#: ../plugins/system-update/GlobalUpdateControls.qml:129
msgid "Update all"
msgstr ""

#: ../plugins/system-update/ImageUpdatePrompt.qml:31
msgid "Update System"
msgstr ""

#: ../plugins/system-update/ImageUpdatePrompt.qml:33
msgid "The device needs to restart to install the system update."
msgstr ""

#: ../plugins/system-update/ImageUpdatePrompt.qml:34
msgid "Connect the device to power before installing the system update."
msgstr ""

#: ../plugins/system-update/ImageUpdatePrompt.qml:39
msgid "Restart & Install"
msgstr ""

#: ../plugins/system-update/ImageUpdatePrompt.qml:49
msgid "Cancel"
msgstr ""

#: ../plugins/system-update/InstallationFailed.qml:29
msgid "OK"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../plugins/system-update/PageComponent.qml:38 settings.js:2
msgid "Updates"
msgstr ""

#: ../plugins/system-update/PageComponent.qml:44
#: ../plugins/system-update/UpdateSettings.qml:32
msgid "Update settings"
msgstr ""

#: ../plugins/system-update/PageComponent.qml:175
#: ../plugins/system-update/ReinstallAllApps.qml:150
msgid "Connect to the Internet to check for updates."
msgstr ""

#: ../plugins/system-update/PageComponent.qml:177
#: ../plugins/system-update/ReinstallAllApps.qml:152
msgid "Software is up to date"
msgstr ""

#: ../plugins/system-update/PageComponent.qml:180
#: ../plugins/system-update/ReinstallAllApps.qml:155
msgid "The update server is not responding. Try again later."
msgstr ""

#: ../plugins/system-update/PageComponent.qml:189
#: ../plugins/system-update/ReinstallAllApps.qml:164
msgid "Updates Available"
msgstr ""

#: ../plugins/system-update/PageComponent.qml:323
msgid "Recent updates"
msgstr ""

#: ../plugins/system-update/ReinstallAllApps.qml:37
#: ../plugins/system-update/ReinstallAllApps.qml:96
#: ../plugins/system-update/UpdateSettings.qml:88
msgid "Reinstall all apps"
msgstr ""

#: ../plugins/system-update/ReinstallAllApps.qml:86
msgid ""
"Use this to get all of the latest apps, typically needed after a major "
"system upgrade, "
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:120
msgid "Retry"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:125
msgid "Update"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:127
msgid "Download"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:133
msgid "Resume"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:140
msgid "Pause"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:144
msgid "Install…"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:146
msgid "Install"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:150
msgid "Open"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:229
msgid "Installing"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:233
msgid "Paused"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:236
msgid "Waiting to download"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:239
msgid "Downloading"
msgstr ""

#. TRANSLATORS: %1 is the human readable amount
#. of bytes downloaded, and %2 is the total to be
#. downloaded.
#: ../plugins/system-update/UpdateDelegate.qml:275
#, qt-format
msgid "%1 of %2"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:279
msgid "Downloaded"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:282
msgid "Installed"
msgstr ""

#. TRANSLATORS: %1 is the date at which this
#. update was applied.
#: ../plugins/system-update/UpdateDelegate.qml:287
#, qt-format
msgid "Updated %1"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:311
msgid "Update failed"
msgstr ""

#: ../plugins/system-update/UpdateSettings.qml:60
msgid "On WiFi"
msgstr ""

#: ../plugins/system-update/UpdateSettings.qml:62
msgid "Always"
msgstr ""

#: ../plugins/system-update/UpdateSettings.qml:64
msgid "Unknown"
msgstr ""

#: ../plugins/system-update/UpdateSettings.qml:70
msgid "Channels"
msgstr ""

#: ../push-helper/software_updates_helper.py:159
msgid "There's an updated system image."
msgstr ""

#: ../push-helper/software_updates_helper.py:160
msgid "Tap to open the system updater."
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:4
msgid "system"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:6
msgid "software"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:8
msgid "update"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:10
msgid "apps"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:12
msgid "application"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:14
msgid "automatic"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:16
msgid "download"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:18
msgid "upgrade"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: settings.js:20
msgid "click"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the update-notification plugin which is used while searching
#: settings.js:22
msgid "Updates available"
msgstr ""
