# Bulgarian translation for lomiri-system-settings-system-update
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the lomiri-system-settings-system-update package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-system-settings-system-update\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-18 11:12-0600\n"
"PO-Revision-Date: 2014-04-01 18:07+0000\n"
"Last-Translator: Atanas Kovachki <Unknown>\n"
"Language-Team: Bulgarian <bg@li.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2015-07-16 05:40+0000\n"
"X-Generator: Launchpad (build 17628)\n"

#: ../plugins/system-update/Configuration.qml:54
#: ../plugins/system-update/UpdateSettings.qml:58
msgid "Never"
msgstr "Никога"

#: ../plugins/system-update/ImageUpdatePrompt.qml:48
msgid "Cancel"
msgstr "Откажи"

#: ../plugins/system-update/UpdateSettings.qml:64
msgid "Unknown"
msgstr ""

#: ../plugins/system-update/InstallationFailed.qml:28
msgid "OK"
msgstr ""

#: ../plugins/system-update/ChangelogExpander.qml:42
#, qt-format
msgid "Version %1"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:32
msgid "Channel settings"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:57
#: ../plugins/system-update/UpdateSettings.qml:74
#, fuzzy
msgid "Stable"
msgstr "Таблет"

#: ../plugins/system-update/ChannelSettings.qml:57
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Release candidate"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:57
#: ../plugins/system-update/UpdateSettings.qml:74
msgid "Development"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:99
msgid "Fetching channels"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:108
msgid "Channel to get updates from:"
msgstr ""

#: ../plugins/system-update/ChannelSettings.qml:124
msgid "Switching channel"
msgstr ""

#: ../plugins/system-update/Configuration.qml:30
#: ../plugins/system-update/UpdateSettings.qml:55
msgid "Auto download"
msgstr "Автоматично сваляне"

#: ../plugins/system-update/Configuration.qml:36
msgid "Download future updates automatically:"
msgstr ""

#: ../plugins/system-update/Configuration.qml:55
msgid "When on WiFi"
msgstr ""

#: ../plugins/system-update/Configuration.qml:57
msgid "On any data connection"
msgstr ""

#: ../plugins/system-update/Configuration.qml:58
msgid "Data charges may apply."
msgstr ""

#: ../plugins/system-update/DownloadHandler.qml:174
#: ../plugins/system-update/InstallationFailed.qml:25
msgid "Installation failed"
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:36
#: ../plugins/system-update/FirmwareUpdate.qml:157
msgid "Firmware Update"
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:133
#, fuzzy
msgid "There is a firmware update available!"
msgstr "Всички налични оформления:"

#: ../plugins/system-update/FirmwareUpdate.qml:134
msgid "Firmware is up to date!"
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:169
msgid "The device will restart automatically after installing is done."
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:185
msgid "Install and restart now"
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:228
msgid ""
"Downloading and Flashing firmware updates, this could take a few minutes..."
msgstr ""

#: ../plugins/system-update/FirmwareUpdate.qml:235
#, fuzzy
msgid "Checking for firmware update"
msgstr "Проверка за актуализации..."

#: ../plugins/system-update/GlobalUpdateControls.qml:84
msgid "Checking for updates…"
msgstr "Проверка за актуализации..."

#: ../plugins/system-update/GlobalUpdateControls.qml:89
msgid "Stop"
msgstr ""

#. TRANSLATORS: %1 is number of software updates available.
#: ../plugins/system-update/GlobalUpdateControls.qml:116
#, fuzzy, qt-format
msgid "%1 update available"
msgid_plural "%1 updates available"
msgstr[0] "Всички налични оформления:"
msgstr[1] "Всички налични оформления:"

#: ../plugins/system-update/GlobalUpdateControls.qml:126
#, fuzzy
msgid "Update all…"
msgstr "Актуализации"

#: ../plugins/system-update/GlobalUpdateControls.qml:128
#, fuzzy
msgid "Update all"
msgstr "Актуализации"

#: ../plugins/system-update/ImageUpdatePrompt.qml:30
msgid "Update System"
msgstr ""

#: ../plugins/system-update/ImageUpdatePrompt.qml:32
msgid "The device needs to restart to install the system update."
msgstr ""

#: ../plugins/system-update/ImageUpdatePrompt.qml:33
msgid "Connect the device to power before installing the system update."
msgstr ""

#: ../plugins/system-update/ImageUpdatePrompt.qml:38
msgid "Restart & Install"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../plugins/system-update/PageComponent.qml:37 ../build/po/settings.js:348
msgid "Updates"
msgstr "Актуализации"

#: ../plugins/system-update/PageComponent.qml:163
#: ../plugins/system-update/ReinstallAllApps.qml:150
msgid "Connect to the Internet to check for updates."
msgstr ""

#: ../plugins/system-update/PageComponent.qml:165
#: ../plugins/system-update/ReinstallAllApps.qml:152
msgid "Software is up to date"
msgstr ""

#: ../plugins/system-update/PageComponent.qml:168
#: ../plugins/system-update/ReinstallAllApps.qml:155
msgid "The update server is not responding. Try again later."
msgstr ""

#: ../plugins/system-update/PageComponent.qml:177
#: ../plugins/system-update/ReinstallAllApps.qml:164
#, fuzzy
msgid "Updates Available"
msgstr "Всички налични оформления:"

#: ../plugins/system-update/PageComponent.qml:311
#, fuzzy
msgid "Recent updates"
msgstr "Провери за актуализации"

#: ../plugins/system-update/PageComponent.qml:374
#: ../plugins/system-update/UpdateSettings.qml:32
#, fuzzy
msgid "Update settings"
msgstr "Системни настройки"

#: ../plugins/system-update/ReinstallAllApps.qml:36
#: ../plugins/system-update/ReinstallAllApps.qml:95
#: ../plugins/system-update/UpdateSettings.qml:88
msgid "Reinstall all apps"
msgstr ""

#: ../plugins/system-update/ReinstallAllApps.qml:85
msgid ""
"Use this to get all of the latest apps, typically needed after a major "
"system upgrade, "
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:119
msgid "Retry"
msgstr "Повторен опит"

#: ../plugins/system-update/UpdateDelegate.qml:124
msgid "Update"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:126
msgid "Download"
msgstr "Изтегляне"

#: ../plugins/system-update/UpdateDelegate.qml:132
msgid "Resume"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:139
msgid "Pause"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:143
msgid "Install…"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:145
msgid "Install"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:149
msgid "Open"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:228
msgid "Installing"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:232
msgid "Paused"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:235
#, fuzzy
msgid "Waiting to download"
msgstr "Автоматично сваляне"

#: ../plugins/system-update/UpdateDelegate.qml:238
msgid "Downloading"
msgstr ""

#. TRANSLATORS: %1 is the human readable amount
#. of bytes downloaded, and %2 is the total to be
#. downloaded.
#: ../plugins/system-update/UpdateDelegate.qml:274
#, qt-format
msgid "%1 of %2"
msgstr ""

#: ../plugins/system-update/UpdateDelegate.qml:278
#, fuzzy
msgid "Downloaded"
msgstr "Изтегляне"

#: ../plugins/system-update/UpdateDelegate.qml:281
msgid "Installed"
msgstr ""

#. TRANSLATORS: %1 is the date at which this
#. update was applied.
#: ../plugins/system-update/UpdateDelegate.qml:286
#, fuzzy, qt-format
msgid "Updated %1"
msgstr "Актуализации"

#: ../plugins/system-update/UpdateDelegate.qml:310
#, fuzzy
msgid "Update failed"
msgstr "Актуализации"

#: ../plugins/system-update/UpdateSettings.qml:60
msgid "On WiFi"
msgstr "Чрез WiFi"

#: ../plugins/system-update/UpdateSettings.qml:62
msgid "Always"
msgstr "Винаги"

#: ../plugins/system-update/UpdateSettings.qml:70
msgid "Channels"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:26
msgid "software"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:108
msgid "automatic"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:210
msgid "click"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:220
msgid "apps"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:350
msgid "system"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:354
msgid "update"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:358
#, fuzzy
msgid "application"
msgstr "Местоположение"

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:362
msgid "download"
msgstr ""

#. TRANSLATORS: This is a keyword or name for the system-update plugin which is used while searching
#: ../build/po/settings.js:364
msgid "upgrade"
msgstr ""

